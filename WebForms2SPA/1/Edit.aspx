﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Edit.aspx.cs" Inherits="WebForms2SPA._1.Edit" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    
      <asp:SqlDataSource runat="server" ID="myData" ConnectionString='<%$ ConnectionStrings:games %>'
        SelectCommand="SELECT [Id], [Name], [PriceUSD], [InStock] FROM [BoardGames] WHERE ([Id] = @Id)" DeleteCommand="DELETE FROM [BoardGames] WHERE [Id] = @Id" InsertCommand="INSERT INTO [BoardGames] ([Name], [PriceUSD], [InStock]) VALUES (@Name, @PriceUSD, @InStock)" UpdateCommand="UPDATE [BoardGames] SET [Name] = @Name, [PriceUSD] = @PriceUSD, [InStock] = @InStock WHERE [Id] = @Id">
        <SelectParameters>
          <asp:QueryStringParameter QueryStringField="id" DefaultValue="0" Name="Id" Type="Int32"></asp:QueryStringParameter>
        </SelectParameters>
        <UpdateParameters>
          <asp:Parameter Name="Name" Type="String"></asp:Parameter>
          <asp:Parameter Name="PriceUSD" Type="Decimal"></asp:Parameter>
          <asp:Parameter Name="InStock" Type="Int32"></asp:Parameter>
          <asp:Parameter Name="Id" Type="Int32"></asp:Parameter>
        </UpdateParameters>
      </asp:SqlDataSource>

      <asp:FormView runat="server" ID="editForm" DefaultMode="Edit" DataSourceID="myData">
        <EditItemTemplate>

          <asp:Label runat="server" Text="ID:" Width="100"></asp:Label>
          <asp:Label runat="server" ID="id" Width="100" Text='<%#: Bind("Id") %>'></asp:Label>
          <br />

          <asp:Label runat="server" Text="Name:" Width="100"></asp:Label>
          <asp:TextBox runat="server" ID="name" Text='<%#: Bind("Name") %>'></asp:TextBox>
          <br />

          <asp:Label runat="server" Text="PriceUSD:" Width="100"></asp:Label>
          <asp:TextBox runat="server" ID="priceUsd" Text='<%#: Bind("PriceUSD") %>'></asp:TextBox>
          <br />

          <asp:Label runat="server" Text="InStock:" Width="100"></asp:Label>
          <asp:TextBox runat="server" ID="inStock" Text='<%#: Bind("InStock") %>'></asp:TextBox>

        </EditItemTemplate>
        <FooterTemplate>
          <asp:Button runat="server" CommandName="Update" Text="Update" />
          <asp:Button runat="server" CommandName="Cancel" Text="Cancel" CausesValidation="false" OnClick="Unnamed_Click" />
        </FooterTemplate>
      </asp:FormView>

    </div>
    </form>
</body>
</html>
