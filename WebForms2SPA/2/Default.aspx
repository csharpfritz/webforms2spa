﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebForms2SPA._2_JustAsBad" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
  <style>
    h2 {
      font-weight: bold;
      text-decoration: underline;
      font-size: 24pt;
    }
  </style>
</head>
<body>
    <form id="form1" runat="server">

      <h2>Board Game Inventory</h2>

      <asp:GridView ID="grid" runat="server" 
        AllowPaging="True"
        AllowSorting="True" OnSorting="grid_Sorting"
        DataKeyNames="Id">
      </asp:GridView>


    </form>
</body>
</html>
