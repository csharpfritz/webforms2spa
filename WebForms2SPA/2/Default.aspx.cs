﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebForms2SPA
{
  public partial class _2_JustAsBad : System.Web.UI.Page
  {

    protected void Page_Load(object sender, EventArgs e)
    {

      if (!Page.IsPostBack)
      {
        var ds = GetData();
        grid.DataSource = ds;
        grid.DataBind();
      }

    }
  
    /// <summary>
    /// Binds the grid.
    /// </summary>
    private DataSet GetData()
    {

      string connectionString = ConfigurationManager.ConnectionStrings["games"].ConnectionString;

      using (var conn = new SqlConnection(connectionString))
      {
        conn.Open();
        var cmd = new SqlCommand("SELECT Id, Name, PriceUSD, InStock FROM BoardGames", conn);
        var da = new SqlDataAdapter(cmd);
        var ds = new DataSet();
        da.Fill(ds);
        return ds;

      }

    }

    public void grid_Sorting(object sender, GridViewSortEventArgs e)
    {

      var ds = GetData();
      var dv = new DataView(ds.Tables[0]);

      if (SortField == e.SortExpression)
      {
        SortDirection = SortDirection == SortDirection.Ascending ? SortDirection.Descending : SortDirection.Ascending;
      }
      SortField = e.SortExpression;

      dv.Sort = string.Concat(SortField, " ", SortDirection == SortDirection.Ascending ? "ASC" : "DESC");
      grid.DataSource = dv;
      grid.DataBind();

    }

    private SortDirection SortDirection
    {
      get {  return (SortDirection)(ViewState["SortDir"] != null ? ViewState["SortDir"] : SortDirection.Ascending); }
      set { ViewState["SortDir"] = value;}
    }

    private string SortField
    {
      get { return ViewState["SortField"] == null ? "" : ViewState["SortField"].ToString(); }
      set { ViewState["SortField"] = value; }
    }


  }
}