﻿<%@ Page Title="Board Game Inventory" Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebForms2SPA._3_Master.Default" MasterPageFile="~/3/3.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="bodyContent">

  <asp:GridView runat="server" ID="grid" 
    UseAccessibleHeader="true"
    SelectMethod="Get"
    AllowSorting="true"
    OnCallingDataMethods="grid_CallingDataMethods"></asp:GridView>


</asp:Content>
