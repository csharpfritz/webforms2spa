﻿<%@ Page Title="Board Game Inventory" Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebForms2SPA._4.Default" MasterPageFile="~/4/4.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="bodyContent">

  <div class="table-responsive">

    <asp:GridView runat="server" ID="grid"
      CssClass="table table-striped" UseAccessibleHeader="true"
      AllowSorting="true"
      SelectMethod="Get">
    </asp:GridView>

  </div>

</asp:Content>
