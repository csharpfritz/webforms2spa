﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebForms2SPA._4a
{
  public partial class Default : System.Web.UI.Page
  {
    Models.BoardGameRepository _Repo = new Models.BoardGameRepository();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    public IQueryable<Models.BoardGame> Get()
    {
      return _Repo.Get();
    }

  }
}