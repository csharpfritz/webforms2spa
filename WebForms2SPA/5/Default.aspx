﻿<%@ Page Title="Board Game Inventory" Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="WebForms2SPA._5.Default" MasterPageFile="~/5/5.Master" %>

<asp:Content runat="server" ContentPlaceHolderID="bodyContent">

  <asp:ScriptManager runat="server" EnablePageMethods="true"></asp:ScriptManager>

  <div class="table-responsive">
  

    <table id="grid" class="table table-striped"">
      <thead>
        <tr>
          <th>ID</th>
          <th>Name</th>
          <th>Price USD</th>
          <th>In Stock</th>
        </tr>
      </thead>
      <tbody data-bind="foreach: games">
        <tr>
          <td data-bind="text: Id"></td>
          <td data-bind="text: Name"></td>
          <td data-bind="text: PriceUSD"></td>
          <td data-bind="text: InStock"></td>
        </tr>
      </tbody>
    </table>

    <script type="text/javascript">

      var myViewModel = {};

      (function() {

        PageMethods.Get(function(result, context) {
          myViewModel.games = ko.observableArray(result);
          console.log(result);
          ko.applyBindings(myViewModel, document.getElementById("grid"));
        });

      })();

    </script>
    
  </div>

</asp:Content>