﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace WebForms2SPA._5
{
  public partial class Default : System.Web.UI.Page
  {

    Models.BoardGameRepository _Repo = new Models.BoardGameRepository();

    protected void Page_Load(object sender, EventArgs e)
    {

    }

    [WebMethod()]
    public static IEnumerable<Models.BoardGame> Get()
    {
      var repo = new Models.BoardGameRepository();
      return repo.Get();
    }

  }
}