﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebForms2SPA._6
{
  public class BoardGameController : ApiController
  {

    private Models.BoardGameRepository _Repo;
    
    public BoardGameController ()
	{
      _Repo = new Models.BoardGameRepository();
	}

    // GET api/<controller>
    public IEnumerable<Models.BoardGame> Get()
    {
      return _Repo.Get();
    }


    // GET api/<controller>/5
    public Models.BoardGame Get(int id)
    {
      return _Repo.Get().FirstOrDefault(b => b.Id == id);
    }

    // POST api/<controller>
    public void Post([FromBody]Models.BoardGame value)
    {

      _Repo.Add(value);

    }

    // PUT api/<controller>/5
    public void Put(int id, [FromBody]Models.BoardGame value)
    {

      _Repo.Update(id, value);

    }

    /*
    // DELETE api/<controller>/5
    public void Delete(int id)
    {
    }
     * 
     * **/

  }
}