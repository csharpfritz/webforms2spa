﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace WebForms2SPA._7
{

  [Queryable]
  public class GameController : ApiController
  {
    // GET api/<controller>
    public IQueryable<Models.BoardGame> Get()
    {
      var repo = new Models.BoardGameRepository();
      return repo.Get();
    }

    public void Post([FromBody]Models.BoardGame game)
    {

      var repo = new Models.BoardGameRepository();
      repo.Update(game.Id, game);

    }

    public void Put([FromBody] Models.BoardGame game)
    {
      var repo = new Models.BoardGameRepository();
      repo.Add(game);
    }
    public void Delete([FromBody] Models.BoardGame game)
    {
      var repo = new Models.BoardGameRepository();
      repo.Delete(game.Id);
    }

  }
}