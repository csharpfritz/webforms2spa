﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebForms2SPA.Models
{

  public class BoardGameRepository : IDisposable
  {

    private gamesEntities _Context;

    public BoardGameRepository()
    {

      this._Context = new gamesEntities();

    }

    #region Cleanup Methods

    ~BoardGameRepository()
    {
      Dispose(false);
    }

    /// <summary>
    /// Performs application-defined tasks associated with freeing, releasing,
    /// or resetting unmanaged resources.
    /// </summary>
    public void Dispose()
    {
      Dispose(true);
    }
    protected virtual void Dispose(bool isDisposing)
    {

      if (isDisposing) GC.SuppressFinalize(this);

      _Context.Dispose();

    }

    #endregion

    public IQueryable<BoardGame> Get()
    {

      return _Context.BoardGames;

    }

    /// <summary>
    /// Adds the specified value.
    /// </summary>
    /// <param name="value">The value.</param>
    public void Add(BoardGame value)
    {
      _Context.BoardGames.Add(value);
      _Context.SaveChanges();
    }


    public void Update(int id, BoardGame value)
    {

      _Context.BoardGames.Attach(value);
      _Context.SaveChanges();

    }

    public void Delete(int id)
    {
      _Context.BoardGames.Remove(_Context.BoardGames.First(b => b.Id == id));
      _Context.SaveChanges();
    }
  }

}